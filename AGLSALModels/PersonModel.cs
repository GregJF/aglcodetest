﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGLSALModels
{
    public class PersonModel
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        
        public List<PetModel> pets { get; set; }
    }
}
