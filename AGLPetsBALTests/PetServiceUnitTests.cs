using AGLPeopleSALInterfaces;
using AGLPetBALInterfaces;
using AGLPetService;
using AGLSALModels;
using AGLWebModels;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGLPetsBALTests
{
    public class PetServiceUnitTests:IPetService
    {
        private IPetService sut;
        private Mock<ILogger<PetService>> loggerMock;
        private Mock<IPeopleSao> peopleSaoMock;

        [SetUp]
        public void Setup()
        {
            loggerMock = new Mock<ILogger<PetService>>();
            peopleSaoMock = new Mock<IPeopleSao>();
            sut = new PetService(loggerMock.Object, peopleSaoMock.Object);
        }

        [Test]
        public async Task TestGetPetsReturnListGenderPetNameModels()
        {
            //arrange
            var personModels = new List<PersonModel>
            {
                new PersonModel
                {
                    gender = "Male",
                    name = "Bill",
                    pets = new List<PetModel>
                    {
                        new PetModel
                        {
                            name ="Fido",
                            type="Cat"
                        },
                        new PetModel
                        {
                            name ="Fluffy",
                            type="Dog"
                        }
                    }
                },new PersonModel
                {
                    gender = "Female",
                    name = "Hepburn",
                    pets = new List<PetModel>
                    {
                        new PetModel
                        {
                            name ="Kate",
                            type="Cat"
                        },
                        new PetModel
                        {
                            name ="Spencer",
                            type="Dog"
                        }
                    }
                },
                new PersonModel
                {
                    gender = "Male",
                    name = "Tracey",
                    pets = new List<PetModel>
                    {
                        new PetModel
                        {
                            name ="Pat",
                            type="Cat"
                        },
                        new PetModel
                        {
                            name ="Mike",
                            type="Dog"
                        }
                    }
                }
            };
            var expected = new List<GenderPetNameModel>
            {
                new GenderPetNameModel
                {
                    Gender = "Male",
                    PetNames =  new List<string>{
                    "Fido","Pat"
                    }
                },
                new GenderPetNameModel
                {
                    Gender = "Female",
                    PetNames =  new List<string>{
                    "Kate"
                    }
                }
            };
            peopleSaoMock.Setup(s => s.GetPets()).ReturnsAsync(personModels);

            //act
            var actual = await GetPets();

            //assert
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.AreEqual(expected[0].PetNames.Count, actual[0].PetNames.Count);
            Assert.AreEqual(expected[1].PetNames.Count, actual[1].PetNames.Count);
            

        }
        public async Task<List<GenderPetNameModel>> GetPets()
        {
            return await sut.GetPets();
        }
    }
}