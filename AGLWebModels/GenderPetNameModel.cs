﻿using System;
using System.Collections.Generic;

namespace AGLWebModels
{
    public class GenderPetNameModel
    {
        public string Gender { get; set; }
        public List<string> PetNames { get; set; }
    }
}
