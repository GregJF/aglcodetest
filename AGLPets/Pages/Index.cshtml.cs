﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using AGLPetBALInterfaces;
using AGLPetWebInterfaces;
using AGLWebModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace AGLPets.Pages
{
    public class IndexModel : PageModel, IPeoplePage
    {
        private readonly ILogger<IndexModel> logger;
        private readonly IPetService petService;

        [BindProperty]
        public List<GenderPetNameModel> genderPetName { get;set;}
        public IndexModel(ILogger<IndexModel> logger, IPetService petService)
        {
            this.logger = logger;
            this.petService = petService;
        }               

        public void OnGet()
        {        
        }
      
        public async Task OnPostPetsOwnerGender()
        {
             await GetPets();
        }
        public async Task GetPets()
        {
            genderPetName = await this.petService.GetPets();
        }
    }
}
