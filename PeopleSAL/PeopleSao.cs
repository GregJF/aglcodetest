﻿using AGLPeopleSALInterfaces;
using AGLSALModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PeopleSAL
{
    public class PeopleSao: IPeopleSao
    {
        private readonly ILogger<PeopleSao> logger;
        private readonly IConfiguration configuration;
        private readonly IHttpClientFactory clientFactory;

        public PeopleSao(ILogger<PeopleSao> logger, IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.clientFactory = clientFactory;
        }
        public async Task<List<PersonModel>> GetPets()
        { 
            var peoplePets = new List<PersonModel>();
            var address = configuration.GetValue<string>("ServiceAddress:AglPeople");
            var request = new HttpRequestMessage(HttpMethod.Get, address);

            var client = this.clientFactory.CreateClient(Constants.AglPeopleService);
            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                this.logger.Log(LogLevel.Information, "ServiceAddress:AglPeople call was successful");
                var content = await response.Content.ReadAsStringAsync();
                 peoplePets = JsonConvert.DeserializeObject<List<PersonModel>>(content);
            }
            else
            {
                this.logger.Log(LogLevel.Warning, "ServiceAddress:AglPeople call was unsuccessful");
            }
            return peoplePets;
        }
    }
}
