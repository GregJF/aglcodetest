﻿using AGLSALModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AGLPeopleSALInterfaces
{
    public interface IPeopleSao
    {
        Task<List<PersonModel>> GetPets();
    }
}
