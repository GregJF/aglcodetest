using AGLPeopleSALInterfaces;
using AGLSALModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using PeopleSAL;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace AGLPetsSALTests
{
    public class PeopleSaoUnitTests: IPeopleSao
    {
        private IPeopleSao sut;
        private Mock<ILogger<PeopleSao>> loggerMock;
        private Mock<IConfiguration> configurationMock;
        private Mock<IHttpClientFactory> clientFactoryMock;
        private Mock<IConfigurationSection> configurationSectionMock;
        private Mock<IHttpClientFactory> httpClientFactoryMock;

        [SetUp]
        public void Setup()
        {
            loggerMock = new  Mock<ILogger<PeopleSao>>();
            configurationMock = new  Mock<IConfiguration>();
         clientFactoryMock = new  Mock<IHttpClientFactory>();
        sut = new PeopleSao(loggerMock.Object, configurationMock.Object, clientFactoryMock.Object);
        }

        [Test]
        public async Task TestGetPetsReturnsListofPersonModels()
        {
            //arrange
            //configurationMock.Setup(a => a.GetValue<string>("ServiceAddress:AglPeople")).Returns("testvalue");

            configurationMock.Setup(c => c.GetSection(It.IsAny<string>())).Returns(new Mock<IConfigurationSection>().Object);

            var stringRet = "[{'name':'Bob','gender':'Male','age':23,'pets':[{'name':'Garfield','type':'Cat'},{'name':'Fido','type':'Dog'}]}," +
                              "{ 'name':'Jennifer','gender':'Female','age':18,'pets':[{ 'name':'Garfield','type':'Cat'}]}," +
                              "{ 'name':'Steve','gender':'Male','age':45,'pets':null}," +
                              "{ 'name':'Fred','gender':'Male','age':40,'pets':[{ 'name':'Tom','type':'Cat'}, { 'name':'Max','type':'Cat'},{ 'name':'Sam','type':'Dog'}, { 'name':'Jim','type':'Cat'}]}," +
                              "{ 'name':'Samantha','gender':'Female','age':40,'pets':[{ 'name':'Tabby','type':'Cat'}]}," +
                              "{ 'name':'Alice','gender':'Female','age':64,'pets':[{ 'name':'Simba','type':'Cat'},{ 'name':'Nemo','type':'Fish'}]}" +
                              "]";

            
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(stringRet),
                });

            //var client = new HttpClient(mockHttpMessageHandler.Object);
            var client = new HttpClient(mockHttpMessageHandler.Object);
            client.BaseAddress = new System.Uri("http://localHost");
            clientFactoryMock.Setup(s => s.CreateClient(It.IsAny<string>())).Returns(client);

            //act
            var actual = await GetPets();

            //assert
            Assert.AreEqual(6,actual.Count);
        }
        public async Task<List<PersonModel>> GetPets()
        {
            return await sut.GetPets();
        }
    }
}

