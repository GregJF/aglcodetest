﻿using AGLPeopleSALInterfaces;
using AGLPetBALInterfaces;
using AGLSALModels;
using AGLWebModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLPetService
{
    public class PetService : IPetService
    {
        private readonly ILogger<PetService> logger;
        private readonly IPeopleSao peopleSao;

        public PetService(ILogger<PetService> logger, IPeopleSao peopleSao)
        {
            this.logger = logger;
            this.peopleSao = peopleSao;
        }
        public async Task<List<GenderPetNameModel>> GetPets()
        {
            var genderPetNameModels = new List<GenderPetNameModel>();
            var peoplePets = await this.peopleSao.GetPets();
            if(peoplePets == null || peoplePets.Any() == false)
            {
                return genderPetNameModels;
            }
            var males = FilterPersonByGender(peoplePets, "Male");
            var malesCatNames = GetUniquePetNameByType(males, "Cat");

            var females = FilterPersonByGender(peoplePets, "Female");
            var femalesCatNames = GetUniquePetNameByType(females, "Cat");
             genderPetNameModels = new List<GenderPetNameModel>
            {
                new GenderPetNameModel
                {
                    Gender = "Male",
                    PetNames = malesCatNames
                },
                new GenderPetNameModel
                {
                    Gender = "Female",
                    PetNames = femalesCatNames
                }
            };
            return genderPetNameModels;
        }
        private List<PersonModel> FilterPersonByGender(List<PersonModel> peoplePets, string genderType)
        {
          return  peoplePets.Select(pp =>
            {
                if (pp.gender.Equals(genderType, StringComparison.InvariantCultureIgnoreCase))
                {
                    return pp;
                }
                return null;
            }).Where(w => w != null).ToList();
        }
        private List<string> GetUniquePetNameByType(List<PersonModel> peoplePets, string petType)
        {
            
            return peoplePets.Where(w => w.pets != null).SelectMany(pp => pp.pets)
                                  .Select(pet => {
                                      if (pet.type.Equals(petType, StringComparison.InvariantCultureIgnoreCase))
                                      {
                                          return pet.name;
                                      }
                                      return null;
                                  })
                                  .Where(w => w != null)
                                  .Distinct()
                                  .OrderBy(o => o)
                                  .ToList();
        }
    }
}
