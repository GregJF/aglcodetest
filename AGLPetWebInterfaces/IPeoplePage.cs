﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AGLPetWebInterfaces
{
    public interface IPeoplePage
    {
        Task GetPets();
    }
}
