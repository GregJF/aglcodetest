using AGLPetWebInterfaces;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using AGLPets.Pages;
using AGLPetBALInterfaces;

namespace AGLPetWebTests
{
    public class PeoplePageUnitTests: IPeoplePage
    {
        private IPeoplePage sut;
        private Mock<ILogger<IndexModel>> loggerMock;
        private Mock<IPetService> petServiceMock;

        [SetUp]
        public void Setup()
        {
            loggerMock = new Mock<ILogger<IndexModel>>();
            petServiceMock = new Mock<IPetService>();
            sut = new IndexModel(loggerMock.Object, petServiceMock.Object);
        }

        [Test]
        public async Task TestGetPetsNoException()
        {
            //arrange

            //act
            await GetPets();

            //assert
            Assert.Pass();
        }
        public async Task GetPets()
        {
            await sut.GetPets();
        }
    }
}