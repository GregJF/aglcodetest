﻿using AGLWebModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AGLPetBALInterfaces
{
    public interface IPetService
    {
        Task<List<GenderPetNameModel>> GetPets();
    }
}
