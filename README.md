# AGL Pets #

This project is a showcase of S.O.L.I.D. principles in practice with T.D.D. as the methodology being use to create a stable testable appliction. 

### What is this repository for? ###

* Quick summary
  
    Use this repository to mimic how a Web Application can be created ad laid out
  
* Version
  
    1.0.0


### How do I get set up? ###

* Summary of set up
  
    This is a .net 3.1 ASP.Net Razor pages application
  
* Configuration
  
    The is no external configuration required.
    
	The appsettings.Deveopment.json (in 30_Presentation/AGLPets project) has an web address to a web service
	
* Dependencies
  
  1. Visual Studio 2019
  2. .net 3.1
  3. nuget packages
  4 .Internet Connection (to get nuget packages)
  
* Database configuration

    The is no database
	
* How to run tests
  
    Open Solution in Visual Studio
	
    Rebuild the Solution. This will retrieve the nuget packages as well.
    
	1. Appliction Test
        1. Run the Appliction
        2. Click button: **Get Pets by Owner Gender**
  
    2. Unit Tests
        1. Open Test Explorer
        2. Run all tests.	 

### Who do I talk to? ###

* Repo owner or admin

    Greg Frazer